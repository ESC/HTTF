HTTF
====

HTTF stands for the 'Hough-Transform Track Finder' and is a fraction of EicRoot track finder/fitter
codes used in particular to simulate the projected performance of the STAR forward upgrade 
silicon strip tracker back in Nov'2015 as well as to optimize the tracker configuration.

The presently extracted part of codes is the recursive tree search algorithm in the parameter space,
which does not depend on the geometry model choice.

It is assumed that the rest of the codes, including geometry interface and the Kalman Filter fitting 
part will be ported and/or adapted once the EIC Software Consortium decides on the portable geometry 
model and sensitive volume definition.

Quick Start
-----------

```
cd /tmp/my-httf
git clone https://gitlab.com/ESC/HTTF.git
cd HTTF
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/tmp/my-httf ..
make 
make install  # optional
```

This installation to the moment only gives one the libhttf.so shared library with the already 
ported portion of track finder codes.
